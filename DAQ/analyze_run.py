#!/usr/bin/python3
from PyQt5.QtWidgets import *
from PyQt5 import uic
from PyQt5.QtCore import QTimer
import sys
import threading
import os
import os.path
import errno
import numpy as np
import stat
import time
import codecs
from queue import Queue
import multiprocessing
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from subprocess import Popen
from datetime import date
from datetime import datetime


class UI(QMainWindow):
    def __init__(self):
        super(UI, self).__init__()
        uic.loadUi("runwindow.ui", self)
        # find the widgets in the xml file
        self.pushButton_select = self.findChild(
            QPushButton, "pushButton_select")
        self.pushButton_select.clicked.connect(self.clk_select)
        self.show()
        self.clk_select()

    
    def clk_select(self):
        plt.close('all')
        fname = QFileDialog.getOpenFileName(self, 'Open file',
                                            'data_output', "Run Files (*.csv)")
        
        print(fname[0])
        df = pd.read_csv(fname[0])
        df['sec'] = df.time - df.time.iloc[0]

        plt.figure()
        sns.distplot(df.adc)

        min_run = (df.time.iloc[-1]-df.time.iloc[0])/60
        avg_min = len(df)/min_run
        print('Run Avg: ' + str(avg_min))

        print('FIRST Event Recorded At (UTC Time): ')
        print(datetime.utcfromtimestamp(
            df.time.iloc[0]).strftime('%Y-%m-%d %H:%M:%S'))

        print('LAST Event Recorded At (UTC Time): ')
        print(datetime.utcfromtimestamp(
            df.time.iloc[-1]).strftime('%Y-%m-%d %H:%M:%S'))

        plt.figure()
        sns.distplot(df.sec)
        plt.title('Events Time Distibution')

        plt.figure()
        adc_cuts = np.linspace(0, 400, 401)
        avg_rate_by_cut = []
        adcs = df.adc.values
        for c in adc_cuts:
            nevts = len(adcs[adcs > c])
            avg_rate_by_cut.append(np.round(nevts/min_run, 3))
        plt.plot(adc_cuts, avg_rate_by_cut)
        plt.ylabel('Rate[1/min]')
        plt.xlabel('ADC Threshold')
        plt.title('Rate(ADC TH)')


        mins = np.arange(0, np.round(df.sec.iloc[-1]/60))
        mins_rate = []
        for m in range(len(mins)):
            mins_rate.append(df.sec[(df.sec > 60*(m-1)) & (df.sec <= (m*60))].count())
        
        mins5 = np.arange(0, np.round(df.sec.iloc[-1]/300))
        mins5_rate = []
        for m in range(len(mins5)):
            mins5_rate.append(df.sec[(df.sec > 300*(m-1)) &
                                    (df.sec <= (m*300))].count()/5)

        fig, ax1 = plt.subplots()
        ax1.plot(mins[5:], mins_rate[5:], lw=0.5, label='1min')
        plt.ylim([50, 250])
        plt.title('Live Rate Full')
        plt.xlabel('Min Elapsed')
        ax2 = ax1.twiny()
        ax2.plot(mins5[1:], mins5_rate[1:], color='C1', lw=0.8, label='5min')
        ax1.legend()
        ax2.legend(loc=2)
        fig.tight_layout()  # otherwise the right y-label is slightly clipped


        plt.show()

    def closeEvent(self, event):
        plt.close('all')
        exit(1)
        
app = QApplication(sys.argv)
window = UI()

app.exec_()
