import ROOT as r
from array import array
import numpy as np
#install this as:
#pip install numpy

f = r.TFile('mppc.root')
t = f.Get('mppc')

chg = array( 'h', [0 for i in range(32) ] )
mac5 = array( 'i', [0] )
ts0 = array('i', [0])

t.SetBranchAddress('mac5',mac5)
t.SetBranchAddress('chg',chg)
t.SetBranchAddress('ts0',ts0)

nent = t.GetEntries()
for i in range(nent):
    t.GetEntry(i)

    #We can now access the ADC values either by
    #chg.at(0..31)
    chg_arr = np.array(chg)

    print(ts0[0])
    print(mac5[0])
    print(chg_arr)


