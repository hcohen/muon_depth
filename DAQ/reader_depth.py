import os
import errno
import numpy as np
import stat
import os


import time

FIFO = '/tmp/dt5702'



while (not os.path.exists(FIFO)) or (not stat.S_ISFIFO(os.stat(FIFO).st_mode)) :
    print('Waiting for pipe')
    time.sleep(1)


with open(FIFO) as fifo:
    print("FIFO opened")
    while True:
        data = fifo.read(1024)
        if len(data) == 0:
            print("Writer closed")
            break
        
        adc = None
        try:
            adc = np.array(data.split(',')[:32]).astype(np.float)
        except:
            pass

        print('Read:')
        print(adc)
