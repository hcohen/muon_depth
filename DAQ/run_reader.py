#!/usr/bin/python3
from PyQt5.QtWidgets import *
from PyQt5 import uic
from PyQt5.QtCore import QTimer
import sys
import threading
import os,os.path
import errno
import numpy as np
import stat
import time
import codecs
from queue import Queue
import multiprocessing
import pandas as pd
from subprocess import Popen
from datetime import date
import datetime


CHANNEL = 2

pipe_running = True
def pipe_reader(ui,q):
    global pipe_running
    FIFO = '/tmp/dt5702'
    while(pipe_running):
        while ( (not os.path.exists(FIFO)) or (not stat.S_ISFIFO(os.stat(FIFO).st_mode)) ) and pipe_running:
            print('|', end="")
            ui.setConnectedStatus(False)
            time.sleep(1)

        if not pipe_running:
            return

        with open(FIFO) as fifo:
            print("FIFO opened")
            ui.setConnectedStatus(True)
            while True:
                data = fifo.read(1040) #Should also read the board's MAC add
                if len(data) == 0:
                    print("Writer closed")
                    break
                
                data = data.split(',')
                mac5 = str(data[0])
                adc = np.array(data[1:33]).astype(np.float)
                
                
                #accept only from single MAC address, the one that is the slave for the AND operation
                if mac5 != ui.comboBox_mac5.currentText():
                    continue

                #Update rates
                if ui.isRunning:
                    q.put([time.time(),adc[CHANNEL]])
                    
 
class UI(QMainWindow):
    def __init__(self):
        super(UI, self).__init__()
        uic.loadUi("mainwindow.ui", self)
 
        # find the widgets in the xml file
        self.pushButton_start = self.findChild(QPushButton, "pushButton_start")
        self.pushButton_start.clicked.connect(self.clk_start)

        self.pushButton_stop = self.findChild(QPushButton, "pushButton_stop")
        self.pushButton_stop.clicked.connect(self.clk_stop)

        self.label_sec = self.findChild(QLabel, 'label_sec')
        self.checkBox_connected = self.findChild(
            QCheckBox, 'checkBox_connected')
        
        self.guiTimer = QTimer(self)
        self.guiTimer.timeout.connect(self.updateGUI)

        self.flushTimer = QTimer(self)
        self.flushTimer.timeout.connect(self.flush_run)

        self.label_rate = self.findChild(QLabel, 'label_rate')
        self.label_60rate = self.findChild(QLabel, 'label_60rate')
        self.label_rate_run = self.findChild(QLabel, 'label_rate_run')
        self.label_depth = self.findChild(QLabel,'label_depth')
        self.label_rate_run_avg = self.findChild(QLabel, 'label_rate_run_avg')
        self.label_timer = self.findChild(QLabel, 'label_timer')
        self.label_filter = self.findChild(QLabel, 'label_filter')

        self.comboBox_mac5 = self.findChild(QComboBox, 'comboBox_mac5')

        self.rates = {'live': 0, 'run': 0,'min': []}
        self.queue = Queue()
        self.isRunning = False

        self.run_data = {'adc':[],'time':[]}

        #Output dir
        t = date.today()
        self.outdir = 'data_output/' + str(t.year) + '_' + str(t.month) + '_' + str(t.day) + '/'
        os.makedirs(self.outdir, exist_ok=True)
        self.lineEdit_prefix = self.findChild(QLineEdit,'lineEdit_prefix')
        self.run_name = ''

        self.nsec = 0
        self.show()


        #Good, now run the readout
        os.system('./kill_readout.sh')
        self.p = Popen(['./run.sh'])


    def get_next_run_name(self):
        ii = 0
        prefix = self.lineEdit_prefix.text()
        if prefix != '':
            prefix+='_'
        while os.path.isfile(self.outdir + 'Run_' + prefix + str(ii)+'.csv'):
            ii+=1

        return self.outdir + 'Run_' + prefix + str(ii)+'.csv'


    def setConnectedStatus(self,status):
        if self.checkBox_connected.isChecked() != status:
            self.checkBox_connected.setChecked(status)


    def updateGUI(self):
        self.nsec += 1

        qs = self.queue.qsize()
        self.rates['live']= qs
        
        #Clear Queue
        for i in range(qs):
            t,adc = self.queue.get()
            self.run_data['adc'].append(adc)
            self.run_data['time'].append(t)

        #update rates
        self.rates['run'] += self.rates['live']
        self.rates['min'].append(self.rates['live'])

        if self.nsec>60:
            last_60 = np.sum((self.rates['min'][-60:]), axis=0)
            self.label_60rate.setText(str(np.round(last_60, 3)))

        #Update labels
        self.label_sec.setText(str(np.round(self.nsec,3)))
        self.label_rate.setText(
            str(np.round(self.rates['live'], 3)))
        self.label_rate_run.setText(str(self.rates['run']))
        self.label_rate_run_avg.setText(str(np.round(self.rates['run']*60/self.nsec,3)))
        self.label_timer.setText(str(datetime.timedelta(seconds=self.nsec)))    

    def clk_start(self):
        if self.isRunning:
            self.flush_run()

        self.nsec =0
        self.nfilter = 0
        self.label_sec.setText('0')
        self.guiTimer.start(1000)
        self.flushTimer.start(1800000)
        self.pushButton_stop.setEnabled(True)
        self.pushButton_start.setText("Reset")
        self.isRunning = True
        self.rates['run'] = 0
        self.label_60rate.setText('0')
        self.run_name = self.get_next_run_name()
        self.lineEdit_prefix.setEnabled(False)
        

    def clk_stop(self):
        self.flush_run()
        self.guiTimer.stop()
        self.flushTimer.stop()
        self.pushButton_start.setText('Start')
        self.pushButton_stop.setEnabled(False)
        self.isRunning = False
        self.lineEdit_prefix.setEnabled(True)
        for r in self.run_data.values():
                r.clear()
    
    def flush_run(self):
        rates = np.array(self.run_data['adc'])
        rates=np.reshape(rates, (rates.shape[0], 1))
        if len(rates) > 0:
            times = np.reshape(np.array(self.run_data['time']),(rates.shape[0],1))

            df = pd.DataFrame(data= np.concatenate((times,rates),axis=1),columns= ['time','adc'])
            df.to_csv(self.run_name,index=False)
            

    def closeEvent(self, event):
        self.clk_stop()
        global pipe_running
        pipe_running=False
        self.p.terminate()
        os.system('./kill_readout.sh')
        event.accept() 
 
 
app = QApplication(sys.argv)
window = UI()

dataTh = threading.Thread(target=pipe_reader, args=(window,window.queue,))
dataTh.start()

app.exec_()
