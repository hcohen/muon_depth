# Run
```
./run_reader
```

# New computer

```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install pip3
python3 -m pip install pandas numpy
git clone https://gitlab.cern.ch/hcohen/muon_depth.git
cd muon_depth/DAQ
source compile
```


Things to validate:
* set up the ethrnet connection, run `ifconfig` and look for `enpXXX`. Copy that to the appropriate place in the `run.sh` script
* edit the user and password to allow super user permissions. Do that in both the `run.sh` and `kill_readout.sh` scripts. Default is `muon` and `telaviv01`.



